﻿using System;
using System.Collections.Generic;
using System.Text;
using Test;
using Test.View;
using Test.ViewModel;

namespace TestApp.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Hello World!";

        public void Show()
        {
            var tw = new TestWindow();
            tw.DataContext = new TestViewModel();
            tw.Show();
        }
    }
}