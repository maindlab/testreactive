using Avalonia;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using Test.ViewModel;

namespace Test.View
{
    public class TestWindow : ReactiveWindow<TestViewModel>
    {
        public TestWindow()
        {
            this.WhenActivated(disposables => { });
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}