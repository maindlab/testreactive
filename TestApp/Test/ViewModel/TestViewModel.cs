using ReactiveUI;

namespace Test.ViewModel
{
    public class TestViewModel : ReactiveObject, IScreen
    {
        public void SetText()=> Router.Navigate.Execute(new TextViewModel(this));
        

        public void SetButton()=> Router.Navigate.Execute(new ButtonViewModel(this));
       
        public RoutingState Router { get; } = new RoutingState();
    }
}