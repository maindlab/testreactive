using System;
using ReactiveUI;

namespace Test.ViewModel
{
    public class TextViewModel:ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment { get; } = Guid.NewGuid().ToString().Substring(0, 5);
        public IScreen HostScreen { get; }
        public TextViewModel (IScreen screen) => HostScreen = screen;
    }
}