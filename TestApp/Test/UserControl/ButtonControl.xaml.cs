using Avalonia;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using Test.ViewModel;

namespace Test.UserControl
{
    public class ButtonControl : ReactiveUserControl<ButtonViewModel>
    {
        public ButtonControl()
        {
            this.WhenActivated(disposables => { });
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}