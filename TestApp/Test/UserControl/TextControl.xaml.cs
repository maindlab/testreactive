using Avalonia;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using Test.ViewModel;

namespace Test.UserControl
{
    public class TextControl : ReactiveUserControl<TextViewModel>
    {
        public TextControl()
        {
            this.WhenActivated(disposables => { });
            InitializeComponent();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}